import { combineReducers } from 'redux';

import { Action } from './types/action';
import { chat } from './services';

export interface GlobalState {
  [chat.NAME]: chat.types.State;
}

const appReducer = combineReducers({
  [chat.NAME]: chat.reducer,
});

export default (state: GlobalState | undefined, action: Action) =>
  appReducer(state, action);
