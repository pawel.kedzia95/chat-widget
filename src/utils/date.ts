import dayjs from 'dayjs';

import DateType from 'types/date';

export const formatDate = (
  date: DateType | number,
  format = 'HH:mm DD/MM/YY',
) => dayjs(date).format(format);
