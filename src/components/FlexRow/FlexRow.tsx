import styled from 'styled-components';

interface FlexRowProps {
  direction?: string;
  justify?: string;
  align?: string;
}

export default styled.div`
  display: flex;
  flex-direction: ${(props: FlexRowProps) => props.direction || 'initial'};
  justify-content: ${(props: FlexRowProps) => props.justify || 'initial'};
  align-items: ${(props: FlexRowProps) => props.align || 'initial'};
`;
