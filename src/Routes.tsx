import React from 'react';
import { Switch, Route } from 'react-router-dom';

import paths from './consts/paths';
import { MainPage } from './scenes';

export const Routes = () => (
  <Switch>
    <Route exact path={paths.mainPage} component={MainPage} />
  </Switch>
);

export default Routes;
