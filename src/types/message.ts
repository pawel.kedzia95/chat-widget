export enum Direction {
  out = 'out',
  in = 'in',
}

export enum Status {
  read = 'read',
  received = 'received',
  sent = 'sent',
}

export interface Message {
  id: number;
  direction: Direction;
  status: Status;
  timestamp: string;
  text: string;
}

export default Message;
