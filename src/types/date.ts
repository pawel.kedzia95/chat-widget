import * as dayjs from 'dayjs';

export type DateType = Date | dayjs.Dayjs;

export default DateType;
