import React from 'react';

import { ChatWidget } from 'containers';

const MainPage = () => (
  <main>
    <ChatWidget />
  </main>
);
export default MainPage;
