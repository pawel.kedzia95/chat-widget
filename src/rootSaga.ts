import { all } from 'redux-saga/effects';

import { chat } from './services';

export default function*() {
  yield all([...chat.sagas]);
}
