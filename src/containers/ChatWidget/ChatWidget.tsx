import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux';

import MessageType from 'types/message';
import { chat as chatService } from 'services';
import { GlobalState } from 'rootReducer';

import { Header, Form, MessagesList } from './components';
import { Container } from './ChatWidget.style';

interface MapStateProps {
  messages: MessageType[];
  sending: boolean;
  fetching: boolean;
  error: null | string;
  unseenMessagesCount: number;
  currentMessage: string;
  lastUnseenMessageIndex: number | null;
}

interface ActionProps {
  getMessages: Function;
  sendMessage: Function;
  setCurrentMessage: Function;
  markMessageAsSeen: Function;
}

type Props = MapStateProps & ActionProps;

const ChatWidget = ({
  sendMessage,
  messages,
  getMessages,
  fetching,
  currentMessage,
  setCurrentMessage,
  markMessageAsSeen,
  unseenMessagesCount,
  lastUnseenMessageIndex,
}: Props) => {
  useEffect(() => {
    getMessages();
  }, [getMessages]);

  const listRef = useRef(null);

  const scrollToElement = (index: number | null) => {
    if (index === null) {
      return;
    }
    const listIndex = index - 1;
    // @ts-ignore
    listRef && listRef.current && listRef.current.scrollToRow(listIndex);
  };

  return (
    <Container>
      <Header unseenMessagesCount={unseenMessagesCount} />
      <MessagesList
        fetching={fetching}
        listRef={listRef}
        list={messages}
        markMessageAsSeen={markMessageAsSeen}
      />
      <Form
        scrollToLastUnseenMessage={() =>
          scrollToElement(lastUnseenMessageIndex)
        }
        currentMessage={currentMessage}
        setCurrentMessage={setCurrentMessage}
        sendMessage={sendMessage}
      />
    </Container>
  );
};

const mapStateToProps = (state: GlobalState): MapStateProps => ({
  messages: chatService.selectors.messages(state),
  sending: chatService.selectors.sending(state),
  fetching: chatService.selectors.fetching(state),
  error: chatService.selectors.error(state),
  currentMessage: chatService.selectors.currentMessage(state),
  unseenMessagesCount: chatService.selectors.unseenMessagesCount(state),
  lastUnseenMessageIndex: chatService.selectors.lastUnseenMessageIndex(state),
});

const mapDispatchToProps: ActionProps = {
  sendMessage: chatService.actions.sendMessage,
  getMessages: chatService.actions.getMessages,
  setCurrentMessage: chatService.actions.setCurrentMessage,
  markMessageAsSeen: chatService.actions.markMessageAsSeen,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatWidget);
