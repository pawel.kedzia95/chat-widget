import styled from 'styled-components';

import { Direction } from 'types/message';

interface ContentProps {
  direction: Direction;
  width: number;
}

export const Content = styled.div`
  background-color: ${(props: ContentProps) =>
    props.direction === 'in' ? '#F1F0F0' : '#0099FF'};
  color: ${(props: ContentProps) =>
    props.direction === 'in' ? 'black' : 'white'};
  width: ${(props: ContentProps) => `${props.width * 0.4}px`};
  font-size: 17px;
  margin: 7px 10px;
  border-radius: 10px;
  padding: 10px 15px 25px 15px;
  position: relative;
  margin-left: ${(props: ContentProps) =>
    props.direction === 'out' ? `${props.width * 0.5}px` : null};
`;

export const Date = styled.span`
  font-size: 11px;
  font-weight: bold;
  margin-right: 5px;
`;

export const Text = styled.p`
  margin: 0px;
  line-height: 1.25;
  word-break: break-word;
`;

export const Info = styled.p`
  position: absolute;
  display: flex;
  margin: 0px;
  right: 10px;
  bottom: 4px;
  align-items: center;
`;
