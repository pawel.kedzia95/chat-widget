import React from 'react';
import {
  IoIosCheckmarkCircleOutline,
  IoIosCheckmarkCircle,
  IoIosCheckmark,
} from 'react-icons/io';

import MessageType, { Status } from 'types/message';
import { formatDate } from 'utils/date';

import { Content, Date, Text, Info } from './Message.style';

type Props = MessageType & { style: any; width: number };

const Checkbox = ({ status }: { status: Status }) => {
  let Icon = null;
  if (status === Status.sent) {
    Icon = <IoIosCheckmark />;
  }
  if (status === Status.received) {
    Icon = <IoIosCheckmarkCircleOutline />;
  }

  if (status === Status.read) {
    Icon = <IoIosCheckmarkCircle />;
  }
  return Icon;
};

const Message = ({
  style,
  direction,
  status,
  timestamp,
  text,
  width,
}: Props) => {
  return (
    <div style={style}>
      <Content direction={direction} width={width}>
        <Text>{text}</Text>
        <Info>
          <Date>{formatDate(Number(timestamp))}</Date>
          {direction === 'out' && <Checkbox status={status} />}
        </Info>
      </Content>
    </div>
  );
};

export default Message;
