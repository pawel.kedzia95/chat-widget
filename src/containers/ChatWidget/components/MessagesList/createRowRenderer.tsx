import React from 'react';
import {
  ListRowRenderer,
  ListRowProps,
  CellMeasurer,
  CellMeasurerCache,
} from 'react-virtualized';

import MessageType from 'types/message';

import { Message } from './components';

export const cache = new CellMeasurerCache({
  defaultHeight: 85,
  fixedWidth: true,
});

export const createRowRenderer = (
  list: MessageType[],
  markMessageAsSeen: Function,
  width: number,
): ListRowRenderer =>
  function rowRenderer({ key, index, isVisible, style, parent }: ListRowProps) {
    const msg = list[index];
    if (isVisible && msg.direction === 'in' && msg.status === 'received') {
      markMessageAsSeen(msg.id);
    }

    return (
      <CellMeasurer
        cache={cache}
        columnIndex={0}
        key={key}
        parent={parent}
        rowIndex={index}
      >
        <Message style={style} width={width} {...msg} />
      </CellMeasurer>
    );
  };
export default createRowRenderer;
