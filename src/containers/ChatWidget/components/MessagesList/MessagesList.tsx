import React, { RefObject } from 'react';
import { List, AutoSizer } from 'react-virtualized';
import Loader from 'react-loader-spinner';

import MessageType from 'types/message';

import { createRowRenderer, cache } from './createRowRenderer';
import {
  LoaderWrapper,
  Container,
  AutoSizerWrapper,
} from './MessagesList.style';
interface Props {
  list: MessageType[];
  fetching: boolean;
  markMessageAsSeen: Function;
  listRef: RefObject<List>;
}

const MessagesList = ({
  list,
  markMessageAsSeen,
  listRef,
  fetching,
}: Props) => {
  const rowCount = list.length;

  if (fetching) {
    return (
      <LoaderWrapper>
        <Loader type="Circles" color="#00BFFF" height={50} width={50} />
      </LoaderWrapper>
    );
  }

  return (
    <Container>
      <AutoSizerWrapper>
        <AutoSizer>
          {({ height, width }) => (
            <List
              width={width}
              height={height}
              rowCount={rowCount}
              rowHeight={cache.rowHeight}
              scrollToIndex={rowCount - 1}
              ref={listRef}
              rowRenderer={createRowRenderer(list, markMessageAsSeen, width)}
            />
          )}
        </AutoSizer>
      </AutoSizerWrapper>
    </Container>
  );
};

export default MessagesList;
