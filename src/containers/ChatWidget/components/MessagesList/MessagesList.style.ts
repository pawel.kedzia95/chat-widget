import styled from 'styled-components';

export const LoaderWrapper = styled.div`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 40%;
  width: 100%;
`;

export const Container = styled.div`
  flex: 1;
`;

export const AutoSizerWrapper = styled.div`
  height: 100%;
`;
