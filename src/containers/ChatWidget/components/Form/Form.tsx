import React, { FormEvent } from 'react';

import {
  FormWrapper,
  IconSend,
  IconUp,
  StyledButton,
  Input,
} from './Form.style';

interface Props {
  currentMessage: string;
  setCurrentMessage: Function;
  sendMessage: Function;
  scrollToLastUnseenMessage: Function;
}

const Form = ({
  currentMessage,
  setCurrentMessage,
  sendMessage,
  scrollToLastUnseenMessage,
}: Props) => {
  const submitForm = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    currentMessage && sendMessage(currentMessage);
  };
  return (
    <FormWrapper onSubmit={submitForm}>
      <StyledButton onClick={() => scrollToLastUnseenMessage()}>
        <IconUp />
      </StyledButton>
      <Input
        type="text"
        placeholder="Send a message..."
        onChange={({ target: { value } }) => setCurrentMessage(value)}
        value={currentMessage}
      />
      <StyledButton type="submit" disabled={!currentMessage}>
        <IconSend />
      </StyledButton>
    </FormWrapper>
  );
};

export default Form;
