import styled from 'styled-components';
import { IoMdSend, IoIosArrowDropupCircle } from 'react-icons/io';

export const FormWrapper = styled.form`
  padding: 10px 20px;
  border: 1px solid #616770;
  border-width: 1px 0px 0px 0px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 60px;
  box-sizing: border-box;
  width: 100%;
`;

export const IconSend = styled(IoMdSend)`
  width: 25px;
  height: 25px;
`;

export const IconUp = styled(IoIosArrowDropupCircle)`
  width: 25px;
  height: 25px;
`;

export const Input = styled.input`
  border-width: 0px;
  flex-grow: 1;
  padding: 10px;
  margin-left: 10px;
  font-size: 17px;
`;

export const StyledButton = styled.button`
  color: ${props => (props.disabled ? '#F1F0F0' : '#0099ff')};
  width: 30px;
  height: 30px;
  background: transparent;
  border-width: 0;
`;
