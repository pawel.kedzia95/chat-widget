import styled from 'styled-components';

export const Wrapper = styled.div`
  height: 60px;
  padding: 10px 20px;
  border: 1px solid #616770;
  border-width: 0px 0px 1px 0px;
  display: flex;
  box-sizing: border-box;
  justify-content: center;
  align-items: center;
`;
