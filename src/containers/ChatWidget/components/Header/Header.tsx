import React from 'react';
import { FaUserNinja } from 'react-icons/fa';

import { Wrapper } from './Header.style';

interface Props {
  unseenMessagesCount: number;
}

const Header = ({ unseenMessagesCount }: Props) => {
  return (
    <Wrapper>
      <FaUserNinja />
      <span>&nbsp; User666 &nbsp; (new messages {unseenMessagesCount})</span>
    </Wrapper>
  );
};

export default Header;
