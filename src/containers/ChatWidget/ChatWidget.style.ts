import styled from 'styled-components';

export const Container = styled.div`
  width: 400px;
  height: 450px;
  border: 1px solid black;
  position: absolute;
  display: flex;
  flex-direction: column;
  bottom: 0;
  right: 10%;
  border-radius: 20px 20px 0px 0px;
  @media (max-width: 500px) {
    border-radius: 0px;
    min-width: 100vw;
    border-width: 0;
    right: 0;
    height: 100vh;
  }
`;
