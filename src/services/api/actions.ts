/* eslint-disable @typescript-eslint/no-unused-vars */
import Message, { Status } from 'types/message';

import chatMessages from './chat.json';

let chatMess = [...chatMessages] as Message[];
export const getMessagesWithUser = (userId: string) => ({
  data: chatMess,
});

export const sendMessage = (text: string, userId: string) => {
  const msg = {
    id: chatMess.length + 1,
    direction: 'out',
    status: 'sent',
    timestamp: Date.now().toString(),
    text,
  } as Message;
  chatMess.push(msg);

  return {
    data: msg,
  };
};

export const markMessageAsSeen = (messageId: number) => {
  let msg = null;
  chatMess = chatMess.map((m: Message) => {
    if (m.id === messageId) {
      msg = {
        ...m,
        status: 'read' as Status,
      };
      return msg;
    }
    return m;
  });
  return { data: msg };
};

export const setMessageSeen = (message: Message): Message => ({
  ...message,
  status: 'read' as Status,
});
