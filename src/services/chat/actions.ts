import { createAction } from 'utils/actions';

import {
  GET_MESSAGES,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_ERROR,
  SEND_MESSAGE,
  SEND_MESSAGE_SUCCESS,
  SEND_MESSAGE_ERROR,
  MARK_MESSAGE_AS_SEEN,
  MARK_MESSAGE_AS_SEEN_SUCCESS,
  MARK_MESSAGE_AS_SEEN_ERROR,
  SET_CURRENT_MESSAGE,
} from './constants';

export const getMessages = createAction(GET_MESSAGES);
export const getMessagesSuccess = createAction(GET_MESSAGES_SUCCESS);
export const getMessagesError = createAction(GET_MESSAGES_ERROR);

export const sendMessage = createAction(SEND_MESSAGE);
export const sendMessageSuccess = createAction(SEND_MESSAGE_SUCCESS);
export const sendMessageError = createAction(SEND_MESSAGE_ERROR);

export const markMessageAsSeen = createAction(MARK_MESSAGE_AS_SEEN);
export const markMessageAsSeenSuccess = createAction(
  MARK_MESSAGE_AS_SEEN_SUCCESS,
);
export const markMessageAsSeenError = createAction(MARK_MESSAGE_AS_SEEN_ERROR);

export const setCurrentMessage = createAction(SET_CURRENT_MESSAGE);
