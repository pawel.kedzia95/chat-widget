import Message from 'types/message';

export interface State {
  readonly messages: Message[];
  readonly currentMessage: string;
  readonly error: string | null;
  readonly fetching: boolean;
  readonly sending: boolean;
}
