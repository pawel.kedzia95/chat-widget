import { call, takeLatest } from 'redux-saga/effects';

import apiCall from 'utils/apiCall';

import * as api from '../api';

import {
  getMessagesSuccess,
  getMessagesError,
  sendMessageSuccess,
  sendMessageError,
  markMessageAsSeenSuccess,
  markMessageAsSeenError,
} from './actions';
import { GET_MESSAGES, SEND_MESSAGE, MARK_MESSAGE_AS_SEEN } from './constants';

export function* getMessagesSaga() {
  yield call(apiCall, {
    method: api.getMessagesWithUser,
    onSuccess: getMessagesSuccess,
    onError: getMessagesError,
    payload: {},
  });
}

interface ISendMessageAction {
  payload: string;
}
export function* sendMessageSaga(action: ISendMessageAction | unknown) {
  const { payload } = action as ISendMessageAction;
  yield call(apiCall, {
    method: api.sendMessage,
    onSuccess: sendMessageSuccess,
    onError: sendMessageError,
    payload: payload,
  });
}

interface IMarkMessageAsSeenAction {
  payload: string;
}
export function* markMessageAsSeenSaga(
  action: IMarkMessageAsSeenAction | unknown,
) {
  const { payload } = action as IMarkMessageAsSeenAction;
  yield call(apiCall, {
    method: api.markMessageAsSeen,
    onSuccess: markMessageAsSeenSuccess,
    onError: markMessageAsSeenError,
    payload: payload,
  });
}

export default [
  takeLatest(SEND_MESSAGE, sendMessageSaga),
  takeLatest(GET_MESSAGES, getMessagesSaga),
  takeLatest(MARK_MESSAGE_AS_SEEN, markMessageAsSeenSaga),
];
