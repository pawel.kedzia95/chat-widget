import { Action } from 'types/action';

import { State } from './types';
import {
  GET_MESSAGES,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_ERROR,
  SEND_MESSAGE,
  SEND_MESSAGE_SUCCESS,
  SEND_MESSAGE_ERROR,
  SET_CURRENT_MESSAGE,
  MARK_MESSAGE_AS_SEEN_SUCCESS,
  MARK_MESSAGE_AS_SEEN_ERROR,
} from './constants';

const initialState: State = {
  error: null,
  currentMessage: '',
  messages: [],
  fetching: false,
  sending: false,
};

export default (state = initialState, { type, payload }: Action): State => {
  switch (type) {
    case GET_MESSAGES:
      return {
        ...state,
        fetching: true,
      };
    case GET_MESSAGES_SUCCESS:
      return {
        ...state,
        fetching: false,
        messages: payload,
        error: null,
      };
    case MARK_MESSAGE_AS_SEEN_SUCCESS:
      return {
        ...state,
        messages: state.messages.map(m => {
          if (m.id === payload.id) {
            return payload;
          }
          return m;
        }),
        error: null,
      };
    case GET_MESSAGES_ERROR:
      return {
        ...state,
        fetching: false,
        error: payload.message,
      };
    case SEND_MESSAGE:
      return {
        ...state,
        sending: true,
      };
    case SEND_MESSAGE_ERROR:
      return {
        ...state,
        sending: false,
        error: payload,
      };
    case MARK_MESSAGE_AS_SEEN_ERROR:
      return {
        ...state,
        error: payload.message,
      };
    case SEND_MESSAGE_SUCCESS:
      return {
        ...state,
        currentMessage: '',
        sending: false,
        error: null,
        messages: [...state.messages, payload],
      };
    case SET_CURRENT_MESSAGE:
      return {
        ...state,
        currentMessage: payload,
      };
    default:
      return state;
  }
};
