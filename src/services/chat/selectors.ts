import { createSelector } from 'reselect';

import { GlobalState } from 'rootReducer';
import Message from 'types/message';

import { State } from './types';
import { NAME } from './constants';

const local = (state: GlobalState): State => state[NAME];

export const messages = createSelector(
  local,
  ({ messages }): Message[] => messages,
);

export const error = createSelector(local, ({ error }) => error);

export const currentMessage = createSelector(
  local,
  ({ currentMessage }) => currentMessage,
);

export const fetching = createSelector(local, ({ fetching }) => fetching);

export const sending = createSelector(local, ({ sending }) => sending);

export const unseenMessages = createSelector(messages, data =>
  data.filter(
    ({ direction, status }) => direction === 'in' && status === 'received',
  ),
);

export const unseenMessagesCount = createSelector(
  unseenMessages,
  data => data.length,
);

export const lastUnseenMessageIndex = createSelector(unseenMessages, data => {
  const msg = data[data.length - 1];
  return (msg && msg.id) || null;
});
